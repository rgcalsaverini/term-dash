#ifndef TERM_DASH_CELL_H
#define TERM_DASH_CELL_H

#include <optional>
#include <memory>
#include <map>
#include <string>

namespace term_dash {
    class Cell {
    public:
        struct RenderedCellDimension {
            int x;
            int y;
            int width;
            int height;
        };
        enum CellDimensionType {
            FIXED_SIZE,
            PCT_SIZE,
            WEIGHT_SIZE,
        };
        enum CellDivisionType {
            ROW,
            COLUMN,
        };
    public:
        Cell(std::string  name, CellDivisionType division, CellDimensionType dimension_type, double value);

        static std::shared_ptr<Cell> make(const std::string& name, CellDivisionType division, CellDimensionType dimension_type, double value);

        void addChildren(const std::string &name, const std::shared_ptr<Cell>& child);

        void resize(CellDimensionType dimension_type, double value);

        std::map<std::string, Cell::RenderedCellDimension> calculate(double x, double y, double width, double height);

        std::pair<bool, double> requiredSpace(double dimension);

        std::string lastChild();

    public:
        // TODO: make private
        std::string _name;

    private:
        std::map<std::string, std::shared_ptr<Cell>> _children;
        CellDivisionType _division;
        CellDimensionType _dimension_type;
        double _value;
        bool _is_leaf = true;
    };
}

#endif //TERM_DASH_CELL_H
