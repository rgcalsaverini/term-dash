#ifndef TERM_DASH_CONTAINER_H
#define TERM_DASH_CONTAINER_H

#include <memory>
#include <map>
#include <string>
#include <vector>
#include "Cell.h"

namespace term_dash {
    class Container {


    public:
        Container(Cell::CellDivisionType division);

        ~Container();

        void addCell(const std::string &parent_name,
                     const std::string &name,
                     Cell::CellDivisionType division,
                     Cell::CellDimensionType dimension_type,
                     double value);

        void resizeCell(const std::string &name, Cell::CellDimensionType dimension_type, double value);

        void recalculate(int x, int y, int width, int height);

        Cell::RenderedCellDimension getLayoutFor(const std::string &name);

        void drawOnPoint(size_t x, size_t y, const std::wstring& str);

        void drawVLine(size_t x, size_t y, size_t height, const std::wstring& str);

        void drawHLine(size_t x, size_t y, size_t width, const std::wstring& str);

        void drawRect(size_t x, size_t y, size_t width, size_t height, const std::wstring& str);

        void flip();

    private:
//        size_t actualStringLength(const std::string &str);

    private:
        std::shared_ptr<Cell> _root_cell;
        std::map<std::string, std::shared_ptr<Cell>> _name_lookup;
        std::map<std::string, Cell::RenderedCellDimension> _layout;
        std::wstring _contents;
        std::wstring _new_contents;
        bool _changed;
        size_t _height = 0;
        size_t _width = 0;

    };
}


#endif //TERM_DASH_CONTAINER_H
