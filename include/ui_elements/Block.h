#ifndef TERM_DASH_BLOCK_H
#define TERM_DASH_BLOCK_H

#include <string>
#include <memory>
#include "../Container.h"

namespace term_dash {
    class Block {
    public:
        explicit Block(bool border = false, bool round_edges = false);

        virtual Cell::RenderedCellDimension renderInto(const std::string &name, Container &container);

    protected:
        bool _border;
        bool _round_edges;
        bool _updated = true;
        std::wstring _corners_straight[4] = {L"┌", L"┐", L"└", L"┘"};
        std::wstring _corners_round[4] = {L"╭", L"╮", L"╰", L"╯"};
    };

}


#endif //TERM_DASH_BLOCK_H
