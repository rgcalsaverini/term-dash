#ifndef TERM_DASH_INDICATORS_H
#define TERM_DASH_INDICATORS_H

#include "Block.h"
#include <string>
#include <map>

namespace term_dash {
    class Indicators : public Block {
    public:
        enum IndicatorStatus {
            INDICATOR_ERROR,
            INDICATOR_WARNING,
            INDICATOR_GOOD,
            INDICATOR_OFF,
        };
    public:
        Indicators(std::map<std::wstring, IndicatorStatus> indicators, bool border = false, bool round_border = false);

        void update(std::map<std::wstring, IndicatorStatus> indicators) { _indicators = indicators; }

        virtual Cell::RenderedCellDimension renderInto(const std::string &name, Container &container);

    protected:
        static std::wstring getColor(IndicatorStatus status);

    protected:
        std::map<std::wstring, IndicatorStatus> _indicators;
    };
};


#endif //TERM_DASH_INDICATORS_H
