#ifndef TERM_DASH_TEXT_H
#define TERM_DASH_TEXT_H

#include "Block.h"
#include <string>

namespace term_dash {

    class Text : public Block {
    public:
        enum TextAlignH {
            ALIGN_LEFT,
            ALIGN_CENTER,
            ALIGN_RIGHT,
        };
        enum TextAlignV {
            ALIGN_TOP,
            ALIGN_MIDDLE,
            ALIGN_BOTTOM,
        };
    public:
        Text(std::wstring text,
             TextAlignH align_h = ALIGN_LEFT,
             TextAlignV align_v = ALIGN_TOP,
             bool border = false,
             bool round_border = false);

        void update(std::wstring text) { _text = text; }

        virtual Cell::RenderedCellDimension renderInto(const std::string &name, Container &container);

    protected:
        std::wstring _text;
        TextAlignH _align_h;
        TextAlignV _align_v;
    };
}


#endif //TERM_DASH_TEXT_H
