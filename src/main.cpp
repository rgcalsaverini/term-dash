#include <utility>

#include "pybind11/pybind11.h"
#include "Container.h"
#include "Cell.h"

namespace py = pybind11;
using namespace pybind11::literals;

void ui_elements(py::module &pybind_module);

term_dash::Cell cell_factory(std::string name,
                            term_dash::Cell::CellDivisionType division_type,
                            term_dash::Cell::CellDimensionType dimension_type,
                            double value) {
    return term_dash::Cell(std::move(name), division_type, dimension_type, value);
}

term_dash::Container container_factory(term_dash::Cell::CellDivisionType division_type) {
    return term_dash::Container(division_type);
}


PYBIND11_MODULE(term_dash, module) {
    module.doc() = "pybind11 example plugin";

    py::module ui_submodule = module.def_submodule(
            "ui", "UI block elements");
    ui_elements(ui_submodule);

    /* term_dash::Cell::CellDivisionType */
    py::enum_<term_dash::Cell::CellDivisionType>(
            module, "CellDivisionType",
            "Enum of division types for a cell")
            .value("column", term_dash::Cell::CellDivisionType::COLUMN)
            .value("row", term_dash::Cell::CellDivisionType::ROW);

    /* term_dash::Cell::CellDimensionType */
    py::enum_<term_dash::Cell::CellDimensionType>(
            module, "CellDimensionType",
            "Enum representing ways that a cell can be dimensioned")
            .value("fixed", term_dash::Cell::CellDimensionType::FIXED_SIZE)
            .value("weight", term_dash::Cell::CellDimensionType::WEIGHT_SIZE)
            .value("percent", term_dash::Cell::CellDimensionType::PCT_SIZE);

    py::class_<term_dash::Container>(
            module, "Container",
            "Container class for organizing the layout")
            .def(py::init(&container_factory), py::arg("division_type") = term_dash::Cell::CellDivisionType::ROW)
            .def("add_cell", &term_dash::Container::addCell)
            .def("resize_cell", &term_dash::Container::resizeCell)
            .def("flip", &term_dash::Container::flip)
            .def("draw_point", &term_dash::Container::drawOnPoint)
            .def("draw_v_line", &term_dash::Container::drawVLine)
            .def("draw_h_line", &term_dash::Container::drawHLine)
            .def("draw_rect", &term_dash::Container::drawRect)
            .def("get_layout_for", [](term_dash::Container &self, const std::string &name) {
                auto layout = self.getLayoutFor(name);
                return py::dict("x"_a = layout.x, "y"_a = layout.x, "width"_a = layout.width,
                                "height"_a = layout.height);
            })
            .def("recalculate", &term_dash::Container::recalculate);

    py::class_<term_dash::Cell>(
            module, "Cell",
            "Layout element of a container, representing a single partition")
            .def(py::init(&cell_factory));


}