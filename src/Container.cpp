#include <iostream>
#include "../include/Container.h"

using term_dash::Container;
using term_dash::Cell;
using std::string;
using std::wstring;
using std::vector;
using std::pair;
using std::to_wstring;

Container::Container(Cell::CellDivisionType division) {
    _root_cell = std::make_shared<Cell>(Cell("_root", division, Cell::CellDimensionType::PCT_SIZE, 1.0));
    _name_lookup.insert({"_root", _root_cell});
    _changed = false;
}

void term_dash::Container::addCell(const string &parent_name,
                                  const string &name,
                                  Cell::CellDivisionType division,
                                  Cell::CellDimensionType dimension_type,
                                  double value) {
    if (_name_lookup.find(parent_name) == _name_lookup.end()) {
        throw std::runtime_error("No parent cell with name '" + parent_name + "'");
    }
    if (_name_lookup.find(name) != _name_lookup.end()) {
        throw std::runtime_error("Cell with name '" + name + "' already exists");
    }
    if (name[0] == '_') {
        throw std::runtime_error("Invalid cell name '" + name + "'");
    }

    auto new_cell = Cell::make(name, division, dimension_type, value);
    _name_lookup[parent_name]->addChildren(name, new_cell);
    _name_lookup.insert({name, new_cell});
}


void term_dash::Container::resizeCell(const std::string &name, Cell::CellDimensionType dimension_type, double value) {
    if (_name_lookup.find(name) == _name_lookup.end()) {
        throw std::runtime_error("Cell with name '" + name + "' does not exist");
    }
    _name_lookup[name]->resize(dimension_type, value);
}


void term_dash::Container::recalculate(int x, int y, int width, int height) {
    _layout.clear();
    _layout = _name_lookup["_root"]->calculate(x, y, width, height);
    _new_contents = wstring((width + 1) * height, ' ');
    _contents = wstring((width + 1) * height, '\0');
    _changed = true;
    _width = width + 1;
    _height = height;
}

Cell::RenderedCellDimension term_dash::Container::getLayoutFor(const string &name) {
    if (_name_lookup.find(name) == _name_lookup.end()) {
        throw std::runtime_error("Cell with name '" + name + "' does not exist");
    }
    return _layout[name];
}

void term_dash::Container::drawOnPoint(size_t x, size_t y, const wstring &str) {
    size_t start = x + y * _width;
    size_t len = str.length();
    if (start + len > _new_contents.length()) return;
    _new_contents.replace(start, len, str);
    _changed = true;
}

void term_dash::Container::drawVLine(size_t x, size_t y, size_t height, const wstring &str) {
    for (int i = 0; i < height; i++) drawOnPoint(x, i + y, str);
}

void term_dash::Container::drawHLine(size_t x, size_t y, size_t width, const wstring &str) {
    wstring whole_line;
    for (int i = 0; i < width; i++) whole_line += str;
    drawOnPoint(x, y, whole_line);
}

void term_dash::Container::drawRect(size_t x, size_t y, size_t width, size_t height, const wstring &str) {
    for (int i = 0; i < height; i++) drawHLine(x, y + i, width, str);
}

void term_dash::Container::flip() {
    if (!_changed) return;

    for (size_t i = 0; i < _new_contents.length(); i++) {
        size_t j = i;
        for (; j < _new_contents.length() && _new_contents[j] != _contents[j] && j % _width < _width - 1; j++);
        if (j > i) {
            wstring new_str = wstring(_new_contents, i, j - i);
            size_t x = i % _width;
            size_t y = i / _width;
            std::wcout << L"\033[" << to_wstring(y + 1) << L";" << to_wstring(x + 1) << L"H" << new_str << L"\n";
            i = j;
        }
    }
//    std::wcout << L"\033[1;1H";
    std::system("tput civis");
    _changed = false;
    _contents = _new_contents;
}

term_dash::Container::~Container() {
    std::system("tput cnorm");
}
