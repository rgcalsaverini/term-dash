#include <iostream>
#include <utility>
#include "../../include/ui_elements/Text.h"

using std::string;
using std::vector;
using std::wstring;
using term_dash::Cell;
using term_dash::Text;

term_dash::Text::Text(wstring text, TextAlignH align_h, TextAlignV align_v, bool border, bool round_border)
        : Block(border, round_border),
          _align_h(align_h),
          _align_v(align_v),
          _text(std::move(text)) {

}

Cell::RenderedCellDimension Text::renderInto(const std::string &name, term_dash::Container &container) {
    auto box = Block::renderInto(name, container);
    int width = box.width - 2;

    vector<wstring> lines;

    for (int line_no = 0; line_no < box.height - 2; line_no++) {
        size_t start = line_no * width;
        if (start >= _text.size()) break;
        wstring line = wstring(_text, start, width);
        if (line.empty()) break;
        lines.push_back(line);
    }
    size_t start_y = box.y + 1;
    if (_align_v == ALIGN_MIDDLE) start_y = box.y + 1 + (box.height - 2 - lines.size()) / 2;
    if (_align_v == ALIGN_BOTTOM) start_y = box.y + 1 + box.height - 2 - lines.size();
    for (int line_no = 0; line_no < lines.size(); line_no++) {
        size_t x = box.x + 1;
        if (_align_h == ALIGN_CENTER) x = box.x + 1 + (width - lines[line_no].length()) / 2;
        if (_align_h == ALIGN_RIGHT) x = box.x + 1 + width - lines[line_no].length();
        container.drawOnPoint(x, start_y + line_no, lines[line_no]);
    }
    return box;
}

