#include "../../include/ui_elements/Indicators.h"

#include <utility>
#include <iostream>

using std::wstring;
using std::string;
using std::move;
using std::map;
using term_dash::Cell;
using term_dash::Indicators;

Indicators::Indicators(map<wstring, IndicatorStatus> indicators, bool border, bool round_border)
        : Block(border, round_border),
          _indicators(move(indicators)) {

}

Cell::RenderedCellDimension Indicators::renderInto(const string &name, term_dash::Container &container) {
    auto box = Block::renderInto(name, container);
    int width = box.width - 2;
    int height = box.height - 2;
    int line_no = 0;

    for (const auto &item : _indicators) {
        if (line_no > height) break;
        wstring bullet = item.second == INDICATOR_OFF ? L"⭘   " : getColor(item.second) + L"⬤   " ;
//        wstring bullet = item.second == INDICATOR_OFF ? L"⭘   " : getColor(item.second) + L"⬤\033[0m   ";
        container.drawOnPoint(box.x + 1, box.y + 1 + line_no, L" " + bullet + wstring(item.first, 0, width - 5));
        line_no++;
    }
    return box;
}

std::wstring term_dash::Indicators::getColor(Indicators::IndicatorStatus status) {
//    if (status == IndicatorStatus::INDICATOR_ERROR) return L"\033[1;31m";
//    if (status == IndicatorStatus::INDICATOR_WARNING) return L"\033[1;33m";
//    if (status == IndicatorStatus::INDICATOR_GOOD) return L"\033[1;34m";
//    return L"\033[1;37m";
    return L"";
}

