#include <iostream>
#include "../../include/ui_elements/Block.h"

using std::to_string;
using std::string;
using std::wstring;
using term_dash::Cell;

term_dash::Block::Block(bool border, bool round_edges) : _border(border), _round_edges(round_edges) {
}

Cell::RenderedCellDimension term_dash::Block::renderInto(const std::string &name, Container &container) {
    auto box = container.getLayoutFor(name);
    container.drawRect(box.x, box.y, box.width, box.height, L" ");

    if (_border) {
        std::wstring horizontal;
        for (int i = 0; i < box.width - 2; i++) horizontal += L"─";
        std::wstring *corners = _round_edges ? _corners_round : _corners_straight;
        container.drawVLine(box.x, box.y + 1, box.height - 2, L"│" + wstring(box.width - 2, ' ') + L"│");
        container.drawOnPoint(box.x, box.y, corners[0] + horizontal + corners[1]);
        container.drawOnPoint(box.x, box.y + box.height - 1, corners[2] + horizontal + corners[3]);
    }
    return box;
}
