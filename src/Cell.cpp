#include "../include/Cell.h"

#include <utility>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>

using term_dash::Cell;
using std::shared_ptr;
using std::make_shared;
using std::string;
using std::map;
using std::pair;
using std::to_string;
using std::min;


term_dash::Cell::Cell(std::string name, Cell::CellDivisionType division, Cell::CellDimensionType dimension_type,
                     double value) :
        _name(std::move(name)),
        _division(division),
        _dimension_type(dimension_type),
        _value(value) {

}

void term_dash::Cell::addChildren(const string &name, const std::shared_ptr<Cell> &child) {
    _children.insert({name, child});
    _is_leaf = false;
}

void term_dash::Cell::resize(Cell::CellDimensionType dimension_type, double value) {
    _dimension_type = dimension_type;
    _value = value;
}

shared_ptr<Cell> Cell::make(const std::string &name,
                            Cell::CellDivisionType division,
                            Cell::CellDimensionType dimension_type,
                            double value) {
    return make_shared<Cell>(name, division, dimension_type, value);
}

map<string, Cell::RenderedCellDimension> Cell::calculate(double x, double y, double width, double height) {
    map<string, RenderedCellDimension> rendered;
    if (_is_leaf) {
        rendered.insert({_name, {
                static_cast<int>(lround(x)),
                static_cast<int>(lround(y)),
                static_cast<int>(lround(width)),
                static_cast<int>(lround(height))}
                        });
        return rendered;
    }
    // Pre allocate
    double dimension = _division == COLUMN ? height : width;
    double space_left = dimension;
    double total_weight = 0;
    double total_non_fixed = 0;
    map<string, pair<bool, double>> child_budget;
    for (const auto &child : _children) {
        auto required_space = child.second->requiredSpace(dimension);
        if (required_space.first) {
            space_left -= required_space.second;
            if (space_left < 0) throw std::runtime_error("No space for cell with name '" + _name + "'");
            child_budget.insert({child.first, {true, required_space.second}});
        } else {
            total_weight += required_space.second;
            total_non_fixed++;
            if (total_non_fixed * 3 > space_left) {
                throw std::runtime_error("No space for cell with name '" + _name + "' on layout");
            }
            child_budget.insert({child.first, {false, required_space.second}});
        }
    }
    // Calculate actual sizes
    double cursor = _division == COLUMN ? y : x;
    double space_per_weight = (double) space_left / (double) total_weight;
    for (const auto &child : _children) {
        auto budget = child_budget[child.first];
        bool last = lastChild() == child.first;
        double child_dimension;
        double start = cursor;

        if (_division == COLUMN) {
            if (last) child_dimension = (int) (height - cursor + y);
            else child_dimension = budget.first ? budget.second : (double) (space_per_weight * budget.second);
            rendered.merge(child.second->calculate(x, cursor, width, child_dimension));

        } else {
            if (last) child_dimension = (int) (width - cursor + x);
            else child_dimension = budget.first ? budget.second : (double) (space_per_weight * budget.second);
            rendered.merge(child.second->calculate(cursor, y, child_dimension, height));
        }
        cursor += child_dimension;


//        if (!child.second->_is_leaf) std::cout << "\033[1;31m";
//        std::cout << std::setw(5) << child.first << ")   ";
//        std::cout << std::setw(6) << (last ? "LAST   " : "       ");
//        std::cout << "w, h = ( " << std::setw(3) << to_string(width) << ", " << std::setw(3) << to_string(height)
//                  << " )    ";
//        std::cout << "x, y = ( " << std::setw(3) << to_string(x) << ", " << std::setw(3) << to_string(y) << " )    ";
//        std::cout << "start = ( " << std::setw(3) << to_string(start) << " )    ";
//        std::cout << "end = ( " << std::setw(3) << to_string(cursor) << " )    ";
//        std::cout << "budget = ( " << std::setw(7) << (budget.first ? "fixed" : "dynamic") << ", "
//                  << to_string(budget.second) << " )";
//        std::cout << "\033[0m\n";
    }

    return rendered;
}

std::pair<bool, double> term_dash::Cell::requiredSpace(double dimension) {
    if (_dimension_type == FIXED_SIZE) return {true, _value};
    if (_dimension_type == PCT_SIZE) return {true, dimension * _value};
    return {false, _value * 100.0};
}

std::string term_dash::Cell::lastChild() {
    std::string last;
    for (const auto &child : _children) last = child.first;
    return last;
}


// lround instead of int



