#include <utility>

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"
#include "ui_elements/Block.h"
#include "ui_elements/Text.h"
#include "ui_elements/Indicators.h"
#include "Container.h"

namespace py = pybind11;
using namespace pybind11::literals;

term_dash::Block block_factory(bool border = false, bool round_edges = false) {
    return term_dash::Block(border, round_edges);
}

term_dash::Text text_factory(const std::wstring &text,
                            term_dash::Text::TextAlignH align_h,
                            term_dash::Text::TextAlignV align_v,
                            bool border,
                            bool round_edges) {
    return term_dash::Text(text, align_h, align_v, border, round_edges);
}

term_dash::Indicators indicator_factory(std::map<std::wstring, term_dash::Indicators::IndicatorStatus> indicators,
                                 bool border,
                                 bool round_edges) {
    return term_dash::Indicators(std::move(indicators), border, round_edges);
}

void ui_elements(py::module &module) {
    py::enum_<term_dash::Text::TextAlignH>(
            module, "TextAlignH",
            "Enum text horizontal alignment")
            .value("left", term_dash::Text::TextAlignH::ALIGN_LEFT)
            .value("center", term_dash::Text::TextAlignH::ALIGN_CENTER)
            .value("right", term_dash::Text::TextAlignH::ALIGN_RIGHT);

    py::enum_<term_dash::Text::TextAlignV>(
            module, "TextAlignV",
            "Enum text vertical alignment")
            .value("left", term_dash::Text::TextAlignV::ALIGN_TOP)
            .value("middle", term_dash::Text::TextAlignV::ALIGN_MIDDLE)
            .value("right", term_dash::Text::TextAlignV::ALIGN_BOTTOM);

    py::enum_<term_dash::Indicators::IndicatorStatus>(
            module, "IndicatorStatus",
            "Indicator statuses")
            .value("error", term_dash::Indicators::IndicatorStatus::INDICATOR_ERROR)
            .value("good", term_dash::Indicators::IndicatorStatus::INDICATOR_GOOD)
            .value("off", term_dash::Indicators::IndicatorStatus::INDICATOR_OFF)
            .value("warning", term_dash::Indicators::IndicatorStatus::INDICATOR_WARNING);

    py::class_<term_dash::Block>(
            module, "Block",
            "A simple block")
            .def(py::init(&block_factory),
                 py::arg("border") = false,
                 py::arg("round_edges") = false
            )
            .def("render_into", [](term_dash::Block &self, const std::string &name, term_dash::Container &container) {
                auto layout = self.renderInto(name, container);
                return py::dict("x"_a = layout.x,
                                "y"_a = layout.x,
                                "width"_a = layout.width,
                                "height"_a = layout.height);
            });

    py::class_<term_dash::Text>(
            module, "Text",
            "A block of text")
            .def(py::init(&text_factory),
                 py::arg("text"),
                 py::arg("align_h") = term_dash::Text::TextAlignH::ALIGN_LEFT,
                 py::arg("align_v") = term_dash::Text::TextAlignV::ALIGN_TOP,
                 py::arg("border") = false,
                 py::arg("round_edges") = false
            )
            .def("update", &term_dash::Text::update)
            .def("render_into", [](term_dash::Text &self, const std::string &name, term_dash::Container &container) {
                auto layout = self.renderInto(name, container);
                return py::dict("x"_a = layout.x,
                                "y"_a = layout.x,
                                "width"_a = layout.width,
                                "height"_a = layout.height);
            });

    py::class_<term_dash::Indicators>(
            module, "Indicators",
            "Some color indicators")
            .def(py::init(&indicator_factory),
                 py::arg("status_map"),
                 py::arg("border") = false,
                 py::arg("round_edges") = false
            )
            .def("render_into", [](term_dash::Indicators &self, const std::string &name, term_dash::Container &container) {
                auto layout = self.renderInto(name, container);
                return py::dict("x"_a = layout.x,
                                "y"_a = layout.x,
                                "width"_a = layout.width,
                                "height"_a = layout.height);
            });
}