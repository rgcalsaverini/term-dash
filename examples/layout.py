from term_dash import Container, CellDivisionType, CellDimensionType
from term_dash.ui import Text, Block, TextAlignH, TextAlignV
import time
import os

col = CellDivisionType.column
row = CellDivisionType.row
division = CellDimensionType.fixed

width = 40
height = 9

os.system('clear')

container = Container(row)
container.add_cell('_root', '1', col, division, width / 2)
container.add_cell('_root', '2', col, division, width / 2)

container.recalculate(0, 0, width, height)
container.draw_rect(0, 0, width, height, '░')
container.flip()
text1 = Text('LEFT', align_h=TextAlignH.center, align_v=TextAlignV.middle, border=True)
text2 = Text('RIGHT', align_h=TextAlignH.center, align_v=TextAlignV.middle, border=True)

i = 3
step = 1
try:
    while True:
        i += step
        if (step > 0 and i >= width - 3) or (step < 0 and i <= 3):
            step = -step
        i = min(width - 3, max(3, i))
        container.resize_cell('1', division, i)
        container.resize_cell('2', division, width - i)

        container.recalculate(0, 0, width, height)

        text1.render_into('1', container)
        text2.render_into('2', container)

        container.flip()
        time.sleep(0.05)
except KeyboardInterrupt:
    pass
