from term_dash import Container, CellDivisionType
import time

c = Container(CellDivisionType.row)
c.recalculate(0, 0, 4, 20)
c.draw_v_line(0, 0, 20, "┃")
c.draw_v_line(2, 0, 20, "┃")
c.draw_point(0, 0, '┏━┓')
c.draw_point(0, 19, '┗━┛')

c.flip()
try:
    i = 0
    i_1 = 0
    i_2 = 0
    i_3 = 0
    i_4 = 0
    while True:
        i_4 = i_3
        i_3 = i_2
        i_2 = i_1
        i_1 = i
        i = (i + 1) % 18
        c.draw_point(1, i_4 + 1, " ")
        c.draw_point(1, i_3 + 1, "░")
        c.draw_point(1, i_2 + 1, "▒")
        c.draw_point(1, i_2 + 1, "▓")
        c.draw_point(1, i + 1, "█")
        c.flip()
        time.sleep(0.05)
except KeyboardInterrupt:
    pass
