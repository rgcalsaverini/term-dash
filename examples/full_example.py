from term_dash import Container, CellDimensionType, CellDivisionType
from term_dash.ui import Block, Text, Indicators, IndicatorStatus, TextAlignH
import os
import time
import datetime


def get_dimensions():
    rows, cols = [int(v) for v in os.popen('stty size', 'r').read().split()]
    return cols, rows - 1


class ExampleApp:
    def __init__(self):
        self._container = Container(CellDivisionType.row)
        self._dimensions = [0, 0, *get_dimensions()]
        self._setup_layout()
        self._last_render = 0
        self._fps = 10
        self._period = 1.0 / self._fps

        self._should_recalculate = True

        # Blocks
        self.block = Block(border=True)
        self.time = Text('--:--:--', border=True, round_edges=True, align_h=TextAlignH.center)
        self.full_date = Text('-')
        self.indicators_1 = Indicators({
            'load balancer 1': IndicatorStatus.good,
            'load balancer 2': IndicatorStatus.good,
            'load balancer bkp': IndicatorStatus.off,
            'ingress proxy': IndicatorStatus.good,
            'ingress proxy bkp': IndicatorStatus.off,
        })
        self.indicators_2 = Indicators({
            'master node 1': IndicatorStatus.good,
            'master node 2': IndicatorStatus.good,
            'master node 3': IndicatorStatus.warning,
            'worker node 1': IndicatorStatus.good,
            'worker node 2': IndicatorStatus.good,
            'worker node 3': IndicatorStatus.warning,
            'worker node 4': IndicatorStatus.error,
            'worker node 5': IndicatorStatus.good,
            'worker node 6': IndicatorStatus.good,
            'worker node 7': IndicatorStatus.good,
            'worker node 8': IndicatorStatus.warning,
            'worker node 9': IndicatorStatus.good,
        })
        self.indicators_3 = Indicators({
            'mongo master': IndicatorStatus.good,
            'mongo slave': IndicatorStatus.good,
            'frontend cdn': IndicatorStatus.good,
            'backup server': IndicatorStatus.off,
        })

    def _setup_layout(self):
        self._container.add_cell('_root', 'left_root', CellDivisionType.column, CellDimensionType.fixed, 30)
        self._container.add_cell('left_root', 'left_1', CellDivisionType.column, CellDimensionType.weight, 1)
        self._container.add_cell('left_root', 'left_2', CellDivisionType.column, CellDimensionType.weight, 2)
        self._container.add_cell('left_root', 'left_3', CellDivisionType.column, CellDimensionType.weight, 1)
        self._container.add_cell('_root', 'right', CellDivisionType.column, CellDimensionType.weight, 1)
        self._container.add_cell('right', 'bar', CellDivisionType.row, CellDimensionType.fixed, 3)
        self._container.add_cell('bar', 'time', CellDivisionType.column, CellDimensionType.fixed, 18)
        self._container.add_cell('bar', 'full_date', CellDivisionType.column, CellDimensionType.weight, 1)
        self._container.add_cell('right', 'empty', CellDivisionType.column, CellDimensionType.percent, 0.2)

    def update_all(self):
        if self._should_recalculate:
            self._container.recalculate(*self._dimensions)
            self._should_recalculate = False
        self.time.render_into('time', self._container)
        self.block.render_into('empty', self._container)
        self.full_date.render_into('full_date', self._container)
        self.indicators_1.render_into('left_1', self._container)
        self.indicators_2.render_into('left_2', self._container)
        self.indicators_3.render_into('left_3', self._container)
        self._container.flip()

    def update_time(self, date_time):
        self.time.update(date_time.strftime('%H:%M:%S'))

        weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        full_date = weekdays[int(date_time.strftime('%w'))]
        full_date += date_time.strftime(', %d %B, %Y')
        self.full_date.update(full_date)

    def tick(self):
        if time.time() - self._last_render < self._period:
            time.sleep(self._period / 2.0)

        new_dimensions = [0, 0, *get_dimensions()]
        if new_dimensions != self._dimensions:
            os.system('clear')
            self._dimensions = new_dimensions
            self._should_recalculate = True
        self.update_all()


if __name__ == '__main__':
    try:
        app = ExampleApp()
        app.update_all()
        while True:
            app.update_time(datetime.datetime.now())
            app.tick()
    except KeyboardInterrupt:
        pass
