# TermDash

A python package written in C++ to help create basic dashboard UIs on the terminal. At it's core sits a 
simple grid layout system that allows for flexibility.

![](docs/layout_.gif)



It is still very early stage, but my goals are basically to make it:
 - Very very fast. Avoiding unnecessary screen updates and implementing it completely in C++
 - Simple to use. Have a nice API that requires as few lines of code as possible
 - Responsive, allowing smooth layout resizing.
 - Portable. Although it is not currently the case. I would only run on linux right now. Maybe even only Ubuntu.
 
This is not intended as a full-fledged text-based user interface framework / toolkit, but rather a library for creating
simple dashboards on the terminal.

## How to use

Install as a dependency with `pip install git+https://gitlab.com/rgcalsaverini/term-dash.git`

Or to run the examples on the [examples folder](./examples) directly, just run `python setup.py install`.
